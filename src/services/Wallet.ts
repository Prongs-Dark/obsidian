import { ethers } from "ethers";
import { ref } from "vue";
import { FANTOM_CHAIN_DETAILS } from "../utils/Constants";

export let ACCOUNT = ref('');
let web3: ethers.providers.Web3Provider | null = null;

export async function connect() {
    if ((window as any).ethereum) {
        web3 = new ethers.providers.Web3Provider((window as any).ethereum);
        await web3.send("eth_requestAccounts", []);

        let address = await web3.getSigner().getAddress();

        const chainId = (await web3.getNetwork()).chainId;
        checkChainId(chainId).then(() => ACCOUNT.value = address)
    }
}

async function checkChainId(chainId: number): Promise<void> {
    if (chainId !== 250) {
        try {
            await web3?.send('wallet_switchEthereumChain', [{ chainId: FANTOM_CHAIN_DETAILS.chainId }])
            return Promise.resolve();
        }
        catch (switchError) {
            if ((switchError as any).code === 4902 || (switchError as any).code === -32603) {
                try {
                    await web3?.send('wallet_addEthereumChain', [FANTOM_CHAIN_DETAILS])
                    return Promise.resolve();
                }
                catch (addError) {
                    return Promise.reject("Chain does not exist on your wallet")
                }
            }
            else {
                return Promise.reject("Chain does not exist on your wallet")
            }
        }
    }
    else {
        return Promise.resolve();
    }
}